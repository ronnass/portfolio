---
cover: "./malmopride-1.jpg"
date: "2015-06-22"
title: "Malmö Pride"
areas:
    - Developer
---
This was a freelance job from Malmö Pride 2015, provided to me via Great Agency. The design was by the agency.