---
cover: "./casinofamily-5.jpg"
date: "2015-11-03"
title: "CasinoFamily"
areas:
    - Developer
    - Designer (2nd iteration)
---
I worked on this site as the sole developer while being employed at MegaHeroes AB. 
It contained both simple and fun interactive tools and more useful ones that could help you in finding online casino sites that suited the user. It also contained a community. The site is no longer active however.

Below you will find the first and second iteration (listed first). 