import React from 'react';
import styled, { keyframes } from 'react-emotion';
import PropTypes from 'prop-types';
import Overdrive from 'react-overdrive';
import { Row, Col } from 'react-flexa';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Constraint } from './Constraint';

const Wrapper = styled.div`
  height: 256px;
  display: flex;
  flex-direction: column;
  position: relative;
  border-bottom: 1px solid rgba(0, 0, 0, 0.125);
  overflow: hidden;
`;

const slide = keyframes`
  0%{
    transform: translate3d(0, 0, 0);
  }
  100%{
    transform: translate3d(0, -1000px, 0);
  }
`;

const Bg = styled.div`
  height: 3000px;
  width: 100%;
  background: url("${props => props.theme.bgPattern}") #fff;
  position: absolute;
  animation: ${slide} 60s linear infinite;
  z-index: -1;
`;

const Intro = styled.div`
  margin: 0 auto;
  max-width: ${props => props.theme.maxWidths.general};
  padding: ${props => props.theme.largeSpacing} ${props => props.theme.smallSpacing};
  color: ${props => props.theme.colors.secondary};
  text-align: center;
`;

const Avatar = styled.div`
  height: 75px;
  width: 75px;
  margin: 0 auto;
  image-rendering: -moz-crisp-edges;
  image-rendering: -o-crisp-edges;
  image-rendering: -webkit-optimize-contrast;
  -ms-interpolation-mode: nearest-neighbor;
`;

const Name = styled.h1`
  margin: 1rem 0 0.25rem;
  color: #525557;
`;

const Tagline = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 0.9rem;
`;

const Link = styled.a`
  display: flex;
  align-items: center;
  margin-bottom: 0.25rem;
  &:hover {
    color: ${props => props.theme.colors.pink};
  }

  @media (min-width: 768px) {
    font-size: 0.75rem;
  }
`;

const StyledFontAwesomeIcon = styled(FontAwesomeIcon)`
  margin-right: 0.5rem;
  color: ${props => props.theme.colors.pink};
`;

const Header = ({ avatar, name, socialMedia, tagline }) => (
  <React.Fragment>
    <Wrapper>
      <Bg />
      <Intro>
        <Overdrive id="avatar-to-back">
          <Avatar>
            <img src={avatar} alt={name} />
          </Avatar>
        </Overdrive>
        <Overdrive id="name-to-back">
          <Name>{name}</Name>
        </Overdrive>
        <Tagline>{tagline}</Tagline>
      </Intro>
    </Wrapper>
    <Constraint width="640" centered pA>
      <Row justifyContent={{ sm: 'center', lg: 'flex-end' }} gutter="16px">
        <Col xs={12} md={8}>
          <h3>About Me</h3>
          <p>
            I'm a front-end developer working with web development. My current work place is Hero Gaming where we're
            using the latest technologies to create WOW experiences. Before this I've worked as a freelance web
            developer and as a indie game developer.
          </p>
        </Col>
        <Col xs={12} md={4}>
          <h3>Contact</h3>
          {socialMedia.map(social => (
            <Link key={social.name} href={social.url} rel="noopener noreferrer">
              <StyledFontAwesomeIcon icon={social.icon} size="2x" />
              <span>{social.name}</span>
            </Link>
          ))}
        </Col>
      </Row>
    </Constraint>
  </React.Fragment>
);

export default Header;

Header.propTypes = {
  avatar: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  tagline: PropTypes.string.isRequired,
  socialMedia: PropTypes.array.isRequired,
};
