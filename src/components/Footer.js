import React from 'react';
import styled from 'react-emotion';

const Wrapper = styled.div`
  padding: ${props => props.theme.smallSpacing};
  display: flex;
  justify-content: center;
  background-color: black;
`;

const Content = styled.p`
  color: ${props => props.theme.colors.secondary};
  text-align: center;
`;

const Footer = () => (
  <Wrapper>
    <Content>
      <a href="mailto:ronny.jn.tenggren@gmail.com">Drop me a line @ ronny.jn.tenggren[at]gmail.com</a>
      <br />
      {'Check me out on '}
      <a href="https://www.linkedin.com/in/ronny-tenggren" target="_blank" rel="noopener noreferrer">
        LinkedIn
      </a>
    </Content>
  </Wrapper>
);

export default Footer;
