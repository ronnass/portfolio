import styled from 'react-emotion';

export const Constraint = styled.div`
  ${props => props.width && `max-width: ${props.width}px`};
  ${props => props.height && `max-height: ${props.height}px`};
  ${props => props.centered && `margin: auto`};
  ${props => props.pH && `padding-left: 16px; padding-right: 16px;`};
  ${props => props.pV && `padding-top: ${props.theme.largeSpacing}; padding-bottom: ${props.theme.smallSpacing};`};
  ${props =>
    props.pA && `padding: ${props.theme.largeSpacing} ${props.theme.smallSpacing} ${props.theme.mediumSpacing}`};
`;
