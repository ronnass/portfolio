import React from 'react';
import styled, { keyframes } from 'react-emotion';
import PropTypes from 'prop-types';
import { Link } from 'gatsby';
import Overdrive from 'react-overdrive';

import arrow from '../images/left-chevron.svg';

const slide = keyframes`
  0%{
    transform: translate3d(0, 0, 0);
  }
  100%{
    transform: translate3d(0, 1000px, 0);
  }
`;

const Wrapper = styled.div`
  min-height: 256px;
  display: flex;
  flex-direction: column;
  position: relative;
  border-bottom: 1px solid rgb(46, 46, 46);
  overflow: hidden;
`;

const Bg = styled.div`
  height: 3000px;
  width: 100%;
  background: url("${props => props.theme.bgPattern2}") ${props => props.theme.colors.black};
  position: absolute;
  bottom: 0;
  animation: ${slide} 60s linear infinite;
  z-index: -1;
`;

const Content = styled.div`
  margin: 0 auto;
  width: 100%;
  max-width: ${props => props.theme.maxWidths.general};
  padding: ${props => props.theme.largeSpacing} ${props => props.theme.largeSpacing};
  color: ${props => props.theme.colors.secondary};
`;

const Back = styled(Link)`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;

  img[data-info='back'] {
    width: 25px;
    height: 25px;
    margin: 0 1rem 0 0;
  }
  &:hover h4 {
    color: ${props => props.theme.colors.linkHover};
    transition: all 0.6s cubic-bezier(0.68, -0.55, 0.265, 1.55);
  }
`;

const Avatar = styled.div`
  height: 40px;
  width: 40px;
  image-rendering: -moz-crisp-edges;
  image-rendering: -o-crisp-edges;
  image-rendering: -webkit-optimize-contrast;
  -ms-interpolation-mode: nearest-neighbor;

  img {
    border-radius: 50%;
  }
`;

const Name = styled.h4`
  margin: 0 0 0 1rem;
  color: ${props => props.theme.colors.link};
`;

const Details = styled.div`
  width: 100%;
  margin-top: 1.5rem;
  text-align: center;

  h1 {
    color: ${props => props.theme.colors.color};
  }

  span {
    &:not(:last-child) {
      margin-right: 0.25rem;
      &:after {
        content: ',';
      }
    }
  }
`;

const Text = styled.div`
  max-width: 750px;
  margin: 2rem auto 0rem;
  color: white;
`;

const ProjectHeader = ({ avatar, name, title, date, areas, text }) => (
  <Wrapper>
    <Bg />
    <Content>
      <Back to="/">
        <img src={arrow} data-info="back" alt="test" />
        <Overdrive id="avatar-to-back">
          <Avatar>
            <img src={avatar} alt={name} />
          </Avatar>
        </Overdrive>
        <Overdrive id="name-to-back">
          <Name>{name}</Name>
        </Overdrive>
      </Back>
      <Details>
        <h1>{title}</h1>
        <p>{date}</p>
        <div>
          {'Role: '}
          {areas.map(area => (
            <span key={area}>{area}</span>
          ))}
        </div>
        {text && <Text dangerouslySetInnerHTML={{ __html: text }} />}
      </Details>
    </Content>
  </Wrapper>
);

export default ProjectHeader;

ProjectHeader.propTypes = {
  avatar: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  areas: PropTypes.array.isRequired,
  text: PropTypes.string.isRequired,
};
