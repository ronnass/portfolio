import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'gatsby';
import styled from 'react-emotion';
import { Row, Col } from 'react-flexa';

import { Card, Header, Layout } from 'components';
import { Constraint } from '../components/Constraint';
import config from '../../config/site';

const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(${props => props.theme.gridColumns}, 1fr);
  grid-gap: 50px;
  margin: 1.5rem 0 3rem;
  padding: 0 ${props => props.theme.smallsSpacing} 1.45rem;
  position: relative;

  @media (max-width: 768px) {
    grid-template-columns: 1fr;
  }

  .gatsby-image-outer-wrapper,
  .gatsby-image-wrapper {
    position: static !important;
  }
`;

const GalleryWrapper = styled.div`
  padding-top: ${props => props.theme.largeSpacing};
  padding-bottom: ${props => props.theme.mediumSpacing};
  background: ${props => props.theme.colors.lightBlack};
`;

const Title = styled.h3`
  text-align: center;
  color: white;
`;

const Index = ({
  data: {
    allMarkdownRemark: { edges },
  },
}) => (
  <Layout>
    <Header avatar={config.avatar} name={config.name} tagline={config.tagline} socialMedia={config.socialMedia} />
    <GalleryWrapper>
      <Constraint width="1000" centered>
        <Row justifyContent={{ lg: 'center' }}>
          <Col xs={12}>
            <Title>Past Works</Title>
            <Grid>
              {edges.map(project => (
                <Card
                  date={project.node.frontmatter.date}
                  title={project.node.frontmatter.title}
                  cover={project.node.frontmatter.cover.childImageSharp.fluid}
                  path={project.node.fields.slug}
                  areas={project.node.frontmatter.areas}
                  slug={project.node.fields.slug}
                  key={project.node.fields.slug}
                />
              ))}
            </Grid>
          </Col>
        </Row>
      </Constraint>
    </GalleryWrapper>
  </Layout>
);

export default Index;

Index.propTypes = {
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      edges: PropTypes.array.isRequired,
    }),
  }).isRequired,
};

export const pageQuery = graphql`
  query HomeQuery {
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            cover {
              childImageSharp {
                fluid(maxWidth: 850, quality: 90, traceSVG: { color: "#328bff" }) {
                  ...GatsbyImageSharpFluid_withWebp_tracedSVG
                }
              }
            }
            date(formatString: "DD.MM.YYYY")
            title
            areas
          }
        }
      }
    }
  }
`;
