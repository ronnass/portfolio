import Typography from 'typography';
import config from '../../config/site';

const typography = new Typography({
  title: 'ronnytenggren',
  baseFontSize: config.baseFontSize,
  baseLineHeight: 1.5,
  headerFontFamily: [config.headerFontFamily, 'sans-serif'],
  bodyFontFamily: [config.bodyFontFamily, 'sans-serif'],
  scaleRatio: 2.5,
  headerWeight: 700,
  headerColor: 'hsla(200,9%,25%,1)',
  marginBottom: 1,
  googleFonts: [
    {
      name: config.headerFontFamily,
      styles: ['700'],
    },
    {
      name: config.bodyFontFamily,
      styles: ['400'],
    },
  ],
  overrideStyles: ({ rhythm }) => ({
    'h1,h2,h3': {
      marginBottom: rhythm(1 / 2),
    },
  }),
});

// Hot reload typography in development.
if (process.env.NODE_ENV !== 'production') {
  typography.injectStyles();
}

export default typography;
