module.exports = {
  pathPrefix: '~ronnyten', // Prefix for all links. If you deploy your site to example.com/portfolio your pathPrefix should be "portfolio"

  siteTitle: 'ronnytenggren',
  siteTitleAlt: 'Ronny Tenggren - Portfolio',
  siteUrl: 'http://98.142.96.242',
  siteLanguage: 'en',
  siteLogo: '/logos/logo.png',
  siteDescription: 'Portfolio of Ronny Tenggren',

  // Manifest and Progress color
  themeColor: '#3498DB',
  backgroundColor: '#2b2e3c',

  // Settings for typography.js
  headerFontFamily: 'Fira Sans',
  bodyFontFamily: 'Montserrat',
  baseFontSize: '16px',

  // Personal information
  avatar: '/logos/logo.png',
  name: 'ronnytenggren',
  tagline: 'Software Developer based in Malmö',
  socialMedia: [
    {
      url: 'https://www.linkedin.com/in/ronny-tenggren',
      name: 'LinkedIn',
      icon: ['fab', 'linkedin'],
    },
    {
      url: 'mailto:ronny.jn.tenggren@gmail.com',
      name: 'Email',
      icon: 'envelope-square',
    },
  ],
};
